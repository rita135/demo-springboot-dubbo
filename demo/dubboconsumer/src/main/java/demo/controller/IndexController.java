package demo.controller;

import com.example.dubbo.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class IndexController {
    @Autowired
    private DemoService dubboService;

    @RequestMapping("/index")
    public String index(){
        System.out.println("true = " + true);
        return "vue";
    }
//
    @RequestMapping("/")
    public String ind(){
        System.out.println("true = " + true);
        return "index";
    }

    @RequestMapping("/demo")
    public String demo(Map<String,Object> map){
        System.out.println("true = " + true);

        map.put("hello","from TemplateController.helloFtl");
        return "demo";
    }


    @RequestMapping("/find")
    public String find(){
        System.out.println("true = " + true);
        String colin = dubboService.sayHello("colin2");
        System.out.println("colin = " + colin);
        return "demo";
    }

}