package com.example.dubbo;

public interface DemoService {
    String sayHello(String name);
}
